![Scheme](jumping-fox.png) 
# Base Test Framework

Base test framework is a simple starting point for your python based automated test framework.
The project consists of the sections:
- application: Here you write all logic for the application you are testing.
- common: The common section contains code that can be shared among all classes. E.g rest client, logger and helpers.
- config: This section contains a jsonfile containing all configurations that later on are converted to a corresponding config class. This class is used throughout the framework and enables the user to easily test against multiple environments.
- test: This section contains your actual tests.

## Coding standards
If you wish to contribute which you are more than welcome to here are a few coding standards to follow.

- Do NOT use constants! Hard coded strings can be configured in the conf.json file.
- Docstring format: Google
- All code here should be as generic and reusabe as possible, please do not add anything that's specific for your project.
- Use Pylint!

## Usage
All examples are configured to run against http://dummy.restapiexample.com/ which is a dummy api.
Note that the first thing I do is to initialize the config object. Using the data in this object I build my base url string which is later on included in EmployeeApi class. The EmployeeApi class is a wrapper for the RestClient class that contains specific methods for the employee endpoint(just a demo). Theoretically new methods for the same endpoint should be added in EmployeeApi. Methods for a new endpoint should NOT be added here, in this case create a new folder and a new class.

- Before you use the framework make sure to install all packages and all corresponding dependencies.
    - E.g requests, psycopg2, pymongo etc.
    - **psycopg2** needs Microsoft Visual C++ 14.00 or higher to run on Windows.

```python
    @classmethod
    def setUpClass(cls):
        cls.conf = Config()
        cls.path_helper = PathHelper(cls.conf)
        url = cls.path_helper.url_builder(cls.conf.rest.base_url,
                                          [cls.conf.rest.api_prefix, cls.conf.rest.version],
                                          ssl=False)
        
        cls.employee_api = EmployeeApi(url, headers={"User-Agent": "XY"})

    def tearDown(self):
        self.employee_api.cleanup_employees()


    def test_get_all_employees(self):
        res = self.employee_api.get_employees()
        self.assertEqual(200, res.status_code)
```
## Executing tests
Go to the scr catalogue in the command line and run:
```
python -m unittest
```

or the following to create a report as well:

```
nosetests --with-xunit
```

## Documentation

The source code documentation can be found in the doc folder. Please go to doc/html/index.html for more info.


## Contributing
For questions contact jan.ogrodowczyk@sogeti.com
