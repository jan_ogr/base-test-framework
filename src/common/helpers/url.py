"""The path helper class is a tool to easily and consistently handle urls and string parameters"""
from os.path import join

from requests.models import PreparedRequest


class PathHelper:
    """Helper class for handling parameterized urls and building url strings.
        E.g url = self.path_helper.add_parameters(self.conf.rest.base_url, {'id': "123456789"})
        url = cls.path_helper.url_builder('https://www.testservice.com',
                                          ['api', 'v1'],
                                          ssl=True)
    """
    def __init__(self, config):
        self.conf = config
        self.req = PreparedRequest()

    def add_parameters(self, url: str, params: dict):
        """
        Creates a url with string parameters based on the params map
        Args:
            url: base url
            params: a dict containing params and their corresponding value

        Returns:
            str, a parameterized url
        """
        self.req.prepare_url(url, params)
        return self.req.url

    def url_builder(self, base_url: str, path: list, ssl=True):
        """
        Generates a url string based on the inputs
        Args:
            base_url: base url string
            path: a list with the relative path elements such as api-prefix and version
            ssl: http or https

        Returns:
            url string

        """
        if self.conf.rest.https not in base_url and ssl:
            base_url = "{}{}".format(self.conf.rest.https, base_url)
        if self.conf.rest.http not in base_url and ssl is False:
            base_url = "{}{}".format(self.conf.rest.http, base_url)

        base_url += ''.join('/%s' % join(m) for m in path)
        return "{}/".format(base_url)
