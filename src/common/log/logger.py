"""Logger class reads from the logging.conf file and creates a logger based on the specifications"""
import logging
import logging.config
import os


class Logger:
    """Basic logger class that initializes a logger based on the logging.conf file.
        E.g
            logger = Logger().get()
            logger.debug('debug message')
            logger.info('info message')
            logger.error('error message')
            logger.critical('critical message')

    """
    def __init__(self, path=None):
        self.file = path if path is not None else \
            "{}\\..\\..\\logging.conf".format(os.path.dirname(os.path.abspath(__file__)))

    def get(self):
        """Creates a logging instance based on the logging.conf file"""
        if os.path.exists(self.file):
            logging.config.fileConfig(self.file)
            return logging.getLogger('root')
        return "Logger file was not found"
