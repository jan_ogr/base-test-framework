"""Common test case class to be inherited from in test classes"""
import unittest


class CommonTestCase(unittest.TestCase):
    """Common test case class, all common steps before a test can/ should be implemented here for consistency.

        Inherit the class as follows.
        E.g
        class MyTestClass(CommonTestCase):
    """
    def setUp(self):
        """Generic setup class"""
        print("Executing test case {}".format(self._testMethodName))

    def tearDown(self):
        """Generic tear down class"""
        print("Finished executing test case {}".format(self._testMethodName))
