"""Generic rest client class implementing a simple logger"""
import time
from urllib.parse import urljoin

import requests
from common.log.logger import Logger


class RestClient(requests.Session):
    """Basic rest client, to be used in all app related methods communicating with a rest api.
        The class is initialized and used as follows.
        Add a base url for easier use in your api classes then only add the relative path for each call.

        class EmployeeApi:

            def __init__(self, base_url, **kwargs):
                self.rest_client = RestClient(base_url, **kwargs)

            def get_employees(self):
                return self.rest_client.get("employees")
    """
    def __init__(self, base_url: str, auth=None, headers=None):
        self.base_url = base_url
        self.session = requests.session()
        self.auth = auth
        self.session.headers = headers
        self.logger = Logger().get()
        super().__init__()

    def post(self, url, data=None, json=None, **kwargs):
        """
        Generic post method
        Args:
            url: relative url
            data: dict
            json: data object

        Returns:
            Response, requests http response object

        """
        return self.session.post(urljoin(self.base_url, url), json=json, **kwargs)

    def get(self, url: str, **kwargs):
        """
        Generic get method
        Args:
            url: relative url

        Returns:
            Response, requests http response object

        """
        return self.session.get(urljoin(self.base_url, url), **kwargs)

    def put(self, url: str, data=None, **kwargs):
        """
        Generic put method
        Args:
            url: relative url
            data: data object

        Returns:
            Response, requests rest response object

        """
        return self.session.put(urljoin(self.base_url, url), data=data, **kwargs)

    def delete(self, url: str, **kwargs):
        """
        Generic delete request
        Args:
            url: relative url

        Returns:
            Response, requests rest response object

        """
        return self.session.delete(urljoin(self.base_url, url), **kwargs)

    def poll(self, url: str, status_code: int, tries: int = 20):
        """

        Args:
            url: relative url
            status_code: expected status code
            tries: number of times to poll with 1 sec break
        Returns:
            Response, requests rest response object if

        """
        for iteration in range(0, tries):
            res = self.session.get(urljoin(self.base_url, url))
            self.logger.debug("Iteration {}, response {}".format(iteration, res.status_code))
            if res.status_code == status_code:
                return res
            time.sleep(1)
        return "Server did not response with expected status code!"
