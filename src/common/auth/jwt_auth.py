"""Wrapper class for jwt"""
import jwt


class JwtAuth:
    """Wrapper class for the pyjwt package. Easier handling of json web tokens. See https://jwt.io/ ."""
    @staticmethod
    def encode_jwt(payload: dict, secret: str, algorithm):
        """
        Returns a jwt authentication header
        Args:
            payload: your payload
            secret: secret string
            algorithm: E.g HS256, HS384 etc

        Returns:
            dict, jwt authorization header
        """
        jwt_base64 = jwt.encode(payload, secret, algorithm=algorithm)
        return {'Authorization': 'Bearer {}'.format(jwt_base64)}

    @staticmethod
    def decode_jwt_header(authorization_header: dict, secret: str, algorithms: list):
        """
        Returns the decoded jwt value, please note that this is a helper method for decoding jwt
        authorization headers and it expects only one ke: value combination.
        Args:
            authorization_header: jwt authorization header
            secret: secret string
            algorithms: list of algorithms e.g HS256, HS384 etc
        Returns:
            decoded jwt dict
        """
        value = list(authorization_header.values())[0]
        base64_jwt = value.replace("Bearer ", "")
        return jwt.decode(base64_jwt, secret, algorithms=algorithms)
