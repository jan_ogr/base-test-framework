"""Wrapper class for psycopg2, an easier way to communicate with postgres db"""
import psycopg2

from common.log.logger import Logger


class PgSql:
    """PostgreSQL wrapper class. Contains all basic operations for communicating with a postgreSQL database."""
    def __init__(self, host: str, database: str, user: str, password: str):
        self.connection = psycopg2.connect(host=host, database=database,
                                           user=user, password=password)
        self.cursor = self.connection.cursor()
        self.logger = Logger().get()

    def insert(self, sql: str, **kwargs):
        """
        Method for inserting data
        Args:
            sql: sql query
        """
        self.cursor.execute(sql, **kwargs)
        self.connection.commit()
        self.logger.info("Data inserted successfully!")

    def select(self, sql: str, **kwargs):
        """
        Method for selecting data
        Args:
            sql: sql query
            **kwargs: Arbitrary keyword arguments.
        Returns:
            [dict]
        """
        self.cursor.execute(sql, **kwargs)
        self.logger.info("Sql queery executed successfully!")
        return self.cursor.fetchall()

    def update(self, sql: str, **kwargs):
        """
        Method for updating data
        Args:
            sql: sql query
            **kwargs: Arbitrary keyword arguments.

        """
        self.cursor.execute(sql, **kwargs)
        self.connection.commit()
        self.logger.info("Data was successfully updated!")

    def delete(self, sql: str, **kwargs):
        """
        Method for deleting data
        Args:
            sql: sql query
            **kwargs: Arbitrary keyword arguments.

        """
        self.cursor.execute(sql, **kwargs)
        self.connection.commit()
        self.logger.info("Data was successfully deleted!")

    def close_connection(self):
        """Method for closing the database connection"""
        self.connection.close()
        self.logger.info("Connection was successfully closed!")
