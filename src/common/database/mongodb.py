"""Pymongo wrapper class"""
import pymongo
from models.config import Config


class Atlas:
    """Wrapper for the pymongo package. Contains all basic operations for communicating with the
    atlas mongodb online database. https://cloud.mongodb.com"""
    def __init__(self, conf: Config, database_name: str):
        self.client = pymongo.MongoClient(conf.database.mongodb_atlas.connection_string)
        self.database = self.client[database_name]

    def insert_one(self, collection: str, data: dict):
        """
        Inserts a data object to the specified collection
        Args:
            collection: name of the collection
            data: data dict object

        Returns:
            returns a dict of the type ObjectId
        """
        col = self.database[collection]
        return col.insert_one(data).inserted_id

    def insert_many(self, collection: str, data: [dict]):
        """
        Inserts a data object to the specified collection
        Args:
            collection: name of the collection
            data: data list of dicts

        Returns:
          [{ObjectId}] returns a list of dicts (type ObjectId)
        """
        col = self.database[collection]
        return col.insert_many(data).inserted_ids

    def find(self, collection: str, data: dict = None):
        """
        Searches for data in the specified collections the data field can be used to filer objects.
        Args:
            collection: name of the collection
            data: data dict, default is set to None. Doesn't have to be used. Filters the data.

        Returns:
          A list of dicts
        """
        col = self.database[collection]
        return [data_object for data_object in col.find(data)]

    def delete_one(self, collection: str, data: dict):
        """
        Deletes one documents matching the data filter
        Args:
            collection: name of the collection
            data: data dict, filters what data to remove
        Returns:
            Int, number of deleted objects
        """
        col = self.database[collection]
        return col.delete_one(data).deleted_count

    def delete_many(self, collection: str, data: dict):
        """
        Deletes all documents matching the data filter
        Args:
            collection: name of the collection
            data: data dict, filters what data to remove

        Returns:
            Int, number of deleted objects
        """
        col = self.database[collection]
        return col.delete_many(data).deleted_count

    def sort(self, collection: str, sort_by: str, data: dict = None, direction: int = 1):
        """
        Searches for data in the specified collections and sorts based on sort_by property.
        Args:
            collection: name of the collection
            sort_by: st
            data: data dict, default is set to None. Doesn't have to be used. Filters the data.
            direction: 1 for ascending, -1 for descending

        Returns:
          A list of dicts
        """
        col = self.database[collection]
        return [data_object for data_object in col.find(data).sort(sort_by, direction)]
