"""
EmployeeAPi class, wrapper for the RestClient class containing employee specific methods.
"""
import json

from common.rest.client import RestClient


class EmployeeApi:
    """" Employee class, example class communicating with the dummy api http://dummy.restapiexample.com"""
    def __init__(self, base_url, **kwargs):
        self.rest_client = RestClient(base_url, **kwargs)
        self.cleanup_list = list()

    def get_employees(self):
        """
        Gets all employees
        Returns:
            Response, requests http response object
        """
        return self.rest_client.get("employees")

    def get_employee_by_id(self, employee_id: str):
        """
        Gets a specific user by id
        Args:
            employee_id: employee specific id

        Returns:
            Response, requests http response object
        """
        return self.rest_client.get("employee/{}".format(employee_id))

    def create_employee(self, json_obj=None):
        """
        Creates an employee
        Args:
            json_obj: employee data object

        Returns:
            Response, requests http response object
        """
        res = self.rest_client.post("create", json=json_obj)
        self.cleanup_list.append(json.loads(res.text))
        return res

    def update_employee(self, employee_id: str, json_obj=None):
        """
        Updates a specific employee
        Args:
            employee_id: employee specific id
            json_obj: data object

        Returns:
            Response, requests http response object
        """
        return self.rest_client.put("update/{}".format(employee_id), json=json_obj)

    def delete_employee(self, employee_id: str):
        """
        Deletes an employee based on id
        Args:
            employee_id: employee specific id

        Returns:
            Response, requests http response object
        """
        return self.rest_client.delete("delete/{}".format(employee_id))

    def cleanup_employees(self):
        """
        Method that deletes all users added in the test session, typically executed
        in the test fixture teardown.
        """
        for user in self.cleanup_list:
            self.delete_employee(user['data']['id'])
        self.cleanup_list.clear()
