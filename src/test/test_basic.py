"""Basic test class, example test class"""
import unittest

from common.auth.jwt_auth import JwtAuth
from models.config import Config
from common.helpers.url import PathHelper
from common.log.logger import Logger
from common.rest.client import RestClient
from common.test.common_test_case import CommonTestCase


class TestBasic(CommonTestCase):
    """Test class containing unit tests for base_test_framework."""
    @classmethod
    def setUpClass(cls):
        cls.conf = Config()
        cls.path_helper = PathHelper(cls.conf)
        url = cls.path_helper.url_builder(cls.conf.rest.base_url,
                                          [cls.conf.rest.api_prefix, cls.conf.rest.version],
                                          ssl=False)

        # Need to change user agent since default is blocked by the website
        cls.rest_client = RestClient(base_url=url, headers={"User-Agent": "XY"})

    def test_logger(self):
        """Basic logger test"""
        logger = Logger().get()
        logger.debug('debug message')
        logger.info('info message')
        logger.error('error message')
        logger.critical('critical message')

        self.assertIsNotNone(logger)

    def test_rest_client(self):
        """Basic rest client test"""
        res = self.rest_client.get("employees")
        self.assertEqual(200, res.status_code)

    def test_parameter_handler(self):
        """Basic parameter handler test"""
        url = self.path_helper.add_parameters(self.conf.rest.base_url, {'lol': "roflmao"})
        self.assertEqual("{}/?lol=roflmao".format(self.conf.rest.base_url), url)

    def test_poll(self):
        """Basic poll test"""
        res = self.rest_client.poll("employees", 200, 5)
        self.assertEqual(200, res.status_code)

    def test_jwt(self):
        secret = "lolololo"
        algorithm = "HS256"
        data = {"sub": "1234567890", "name": "John Doe", "iat": 1516239022}
        header = JwtAuth.encode_jwt(data, secret, algorithm)

        decode = JwtAuth.decode_jwt_header(header, secret, algorithms=[algorithm])
        self.assertEqual(data, decode)


def main():
    """Main method for executing test class"""
    unittest.TextTestRunner().run(TestBasic())


if __name__ == "__main__":
    unittest.main()
