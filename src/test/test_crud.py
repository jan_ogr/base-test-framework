"""TestCrud Class, a class for Employee related crud tests
executed against a dummy api"""
import json
import unittest

from parameterized import parameterized

from models.config import Config
from app.employee.api import EmployeeApi
from common.helpers.url import PathHelper
from common.test.common_test_case import CommonTestCase


class TestCrud(CommonTestCase):
    """Example class on how to set up api tests. """
    @classmethod
    def setUpClass(cls):
        cls.conf = Config()
        cls.path_helper = PathHelper(cls.conf)
        url = cls.path_helper.url_builder(cls.conf.rest.base_url,
                                          [cls.conf.rest.api_prefix, cls.conf.rest.version],
                                          ssl=False)
        # Need to change user agent since default is blocked by the website
        cls.employee_api = EmployeeApi(url, headers={"User-Agent": "XY"})

    @classmethod
    def tearDownClass(cls):
        """Teardown, executes the cleanup method"""
        cls.employee_api.cleanup_employees()

    def test_get_all_employees(self):
        """A test fetching all employees"""
        res = self.employee_api.get_employees()
        self.assertEqual(200, res.status_code)

    def test_get_specific_user(self):
        """A test fetching a specific user"""
        res = self.employee_api.get_employees()
        self.assertEqual(200, res.status_code)

        employees = json.loads(res.text)
        res = self.employee_api.get_employee_by_id(employees['data'][0]['id'])
        self.assertEqual(200, res.status_code)

    @parameterized.expand([('lilla gubbenhej', '10', '10'), ('Herr NILSSON', '1000000', '3')])
    def test_create_user(self, name, salary, age):
        """Data driven tests for creating users"""
        employee = {'name': name, 'salary': salary, 'age': age}
        res = self.employee_api.create_employee(json_obj=employee)
        self. assertEqual(200, res.status_code)

        res_employee = json.loads(res.text)
        self.assertEqual(name, res_employee['data']['name'])
        self.assertEqual(salary, res_employee['data']['salary'])
        self.assertEqual(age, res_employee['data']['age'])


def main():
    unittest.TextTestRunner().run(TestCrud())


if __name__ == "__main__":
    unittest.main()
