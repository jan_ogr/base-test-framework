"""Config class"""
import json
import os


class Config:
    """Config class reading the config,json file and mapping it against its properties. If the json file is updated
        make sure to update the config class with the corresponding changes.

        Bellow you'll find a peice of code describing how to use the config file.
        E.g
            cls.conf = Config()
            cls.path_helper = PathHelper(cls.conf)
            url = cls.path_helper.url_builder(cls.conf.rest.base_url,
                                          [cls.conf.rest.api_prefix, cls.conf.rest.version],
                                          ssl=False)
    """
    def __init__(self):
        data = self.get()
        self.rest = Rest(data)
        self.database = Database(data)

    @staticmethod
    def get():
        """reads the config.json and converts it to a dict."""
        file = "{}\\..\\conf.json".format(os.path.dirname(os.path.abspath(__file__)))
        if os.path.exists(file):
            with open(file, "r+") as conf_file:
                read_file = json.load(conf_file)
            conf_file.close()
            return read_file
        return "Could not locate conf.json file!"


class Rest:
    """Rest class"""
    def __init__(self, data):
        self.http = data['rest']['http']
        self.https = data['rest']['https']
        self.base_url = data['rest']['base_url']
        self.version = data['rest']['version']
        self.api_prefix = data['rest']['api_prefix']


class Database:
    """Database class"""
    def __init__(self, data):
        self.mongodb_atlas = MongodbAtlas(data)


class MongodbAtlas:
    """Mongodb Atlas class"""
    def __init__(self, data):
        self.connection_string = data['database']['mongodb_atlas']['connectionString']
