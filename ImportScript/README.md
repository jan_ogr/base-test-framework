# ImportScript

This folder contains two separate import scripts one targeting an atlas mongo db instance, the second one aimed at a local postgres database. The idea is to save all the test executions and analyze the data either using the built in atlas data visualizer or using a tool installed on your local network (import_data_postgres.py). If you do not wish to save the test meta data remove the sections listed bellow from your Jenkinsfile. Please note that these scripts are written for a Jenkins instance running on a windows machine, in order to run them on a Linux environment you need to add the shebang and change every 'bat' command to a corresponding 'sh' command in the Jenkinsfile. 

## Import_data
In order to use this script you need to either define the following Jenkins credentials or provide them one way or another to the script. The API_KEY issued for fetching the test data from each Jenkins execution. USER holds the jenkins user username. The other two for communicating with your atlas mongo db instance.
```
		API_KEY = credentials('API_KEY')
		MONGODB_PSW = credentials('MONGODB_PSW')
		SERVER = credentials('SERVER')
		USER = credentials('USER')
```

##### Execute the script as follows. 

##### Windows:
#
```
    python import_data.py %BUILD_NUMBER% %USER% %API_KEY% %MONGODB_PSW% %SERVER%
```
#### Linux:
#
```
    python import_data.py ${BUILD_NUMBER} ${USER} ${API_KEY} ${MONGODB_PSW} ${SERVER}
```

## import_data_postgres
The postgres version also needs a number of parameters being passed into the script:

```
        USER = credentials('USER')
		API_KEY = credentials('API_KEY')
		PG_PASS = credentials('PG_PASS')
```

##### Windows:
#
```
    python import_data_postgres.py %BUILD_NUMBER% %USER% %API_KEY% %PG_PASS%
```
#### Linux:
#
```
    python import_data_postgres.py ${BUILD_NUMBER} ${USER} ${API_KEY} ${PG_PASS}
```