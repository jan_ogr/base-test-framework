"""
Script for importing the latest test execution result to an atlas mongo db instance.
"""
import sys
import json

import requests
import pymongo


def main():
    """Executes the script main method, fetching latest test data and inserting to the database."""
    build_number = sys.argv[1]
    user_name = sys.argv[2]
    password = sys.argv[3]
    mongodb_psw = sys.argv[4]
    server = sys.argv[5]

    client = pymongo.MongoClient("mongodb+srv://mongodb:{}@{}/mdb?retryWrites=true&w=majority"
                                 .format(mongodb_psw, server))
    database = client['mdb']
    col = database['results_python']

    res = requests.get(
        "http://localhost:8080/job/Base_test_framework_python/job/master/{}/testReport/api/json?"
        "pretty=true".format(build_number), auth=(user_name, password))
    response_data = json.loads(res.content)

    for test in response_data['suites'][0]['cases']:
        try:
            test['buildNumber'] = build_number
            col.insert_one(test)
            print("Data for test {} inserted successfully!".format(test['name']))
        except Exception as ex:
            print(ex)


if __name__ == "__main__":
    main()
