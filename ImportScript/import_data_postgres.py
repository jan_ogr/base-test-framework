"""
Script for importing the latest test execution result to a postgres database table called results.
"""
import sys
import json

import requests
import psycopg2


def main():
    """Executes the script main method, fetching latest test data and inserting to the database."""
    build_number = sys.argv[1]
    user_name = sys.argv[2]
    password = sys.argv[3]
    db_password = sys.argv[4]

    connection = psycopg2.connect(
        host="localhost",
        database="postgres",
        user="postgres",
        password=db_password)
    cursor = connection.cursor()
    res = requests.get(
        "http://localhost:8080/job/Base_test_framework_python/job/master/{}/testReport/api/json?"
        "pretty=true".format(build_number), auth=(user_name, password))
    response_data = json.loads(res.content)

    for test in response_data['suites'][0]['cases']:
        try:
            cursor.execute('INSERT INTO public.results (build_nr,age,class_name,duration,'
                           'error_details,failed_since,test_name,skipped,status) VALUES '
                           '(%s, %s, \'%s\', %s, \'%s\', %s, \'%s\', %s, \'%s\')' %
                           (build_number, test['age'], test['className'], test['duration'],
                            test['errorDetails'], test['failedSince'], test['name'],
                            test['skipped'], test['status']))
            print("Data for test {} inserted successfully!".format(test['name']))
        except Exception as ex:
            print(ex)

    connection.commit()


if __name__ == "__main__":
    main()
